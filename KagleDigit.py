# работа с массивами
import numpy as np

# для одной маленькой функции
import random

# визуализация
from matplotlib import pyplot as plt
from matplotlib import cm

# нейронная сеть
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.constraints import maxnorm
from keras.optimizers import SGD
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras.models import load_model
from keras import backend as K

# использование в качестве backend TensorFlow
K.set_image_dim_ordering('th')

class Net:
    def __init__(self):
        """
        Инициация класса, ну и как следствие сети
        :return: None
        """
		# инициализация сети
        self.net = Sequential()
        # первый метаслой свёртки ()
		self.net.add(Convolution2D(32, 3, 3, input_shape=(1, 28, 28), activation='relu', border_mode='same'))
        self.net.add(Convolution2D(64, 3, 3, activation='relu', border_mode='same'))
        self.net.add(MaxPooling2D(pool_size=(2, 2)))
		# регуляризация сети        
		self.net.add(Dropout(0.2))
		# второй метаслой свёртки        
		self.net.add(Convolution2D(64, 3, 3, activation='relu', border_mode='same'))
        self.net.add(MaxPooling2D(pool_size=(2, 2)))
		# третий метаслой свёртки        
		self.net.add(Convolution2D(128, 3, 3, activation='relu', border_mode='same'))
        self.net.add(Convolution2D(128, 3, 3, activation='relu', border_mode='same'))
        self.net.add(MaxPooling2D(pool_size=(2, 2)))
		# слой преобразующий карты признаков в одномерный массив
		self.net.add(Flatten())
		# регуляризация сети          
		self.net.add(Dropout(0.2))
		# 2 слоя полносвязной нейронной сети       
		self.net.add(Dense(64, activation='relu', W_constraint=maxnorm(3)))
        self.net.add(Dropout(0.2))
        self.net.add(Dense(64, activation='relu', W_constraint=maxnorm(3)))
        self.net.add(Dropout(0.2))
		# выходной слой 10 нейронов, каждый из которых реагирует на определенную цифру        
		self.net.add(Dense(10, activation='softmax'))
		
		# параметры обучения
        self.epochs = 25
        lrate = 0.01
        decay = lrate / self.epochs
        sgd = SGD(lr=lrate, momentum=0.9, decay=decay, nesterov=False)
        self.net.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
        print(self.net.summary())

        return


    def prepare_dataset_from_csv(self, path, name_train, name_test):
        """
        Подготавлеваем данные, для того чтобы скормить их нейронной сети

        :param path: путь до файлов .csv датасета
        :param name_train: имя файла с обучающим датасетом
        :param name_test: имя фала с тестовым датасетом
        :return: None
        """
        # выставляем дефолтные значения
        if path is None:
            path = ''

        if name_test is None:
            name_test = 'test.csv'

        if name_train is None:
            name_train = 'train.csv'

        # тащим данные из csv
        data = np.genfromtxt((str(path) + str(name_train)), delimiter=',')
        self.X_test = np.genfromtxt((str(path) + str(name_test)), delimiter=',')

        # валидационный сет бурем из тренировачного, я взял 1/40 (образцы случайно размешаны в сете,
        # можно больше не перемешивать) 1 столбец - значение y
        self.X_val = data[:1000, 1:]
        self.y_val = data[:1000, 0]
        self.X_train = data[1000:, 1:]
        self.y_train = data[1000:, 0]

        # приводим к квадратному виду (-1 значит, что перебираются все образцы)
        self.X_train = self.X_train.reshape((-1, 1, 28, 28))
        self.X_val = self.X_val.reshape((-1, 1, 28, 28))
        self.X_test = self.X_test.reshape((-1, 1, 28, 28))

        # значения y должны быть unit8 - так в lasagne зашито
        self.y_train = self.y_train.astype(np.uint8)
        self.y_val = self.y_val.astype(np.uint8)

        self.y_train = np_utils.to_categorical(self.y_train)
        self.y_val = np_utils.to_categorical(self.y_val)

        # возвращаем сеты
        return


    def random_image(self, number):
        """
        Рисуем случайную картинку (для проверки)

        :param number: номер картинки  от 0 до 30к
        :return: None
        """

        # выставляем дефолтные значения
        if number is None:
            number = random.randrange(0, 30000)

        plt.imshow(self.X_train[number][0], cmap=cm.binary)
        return


    def train_or_load_net(self, path, name, flag):
        """
        Обучаем сеть или загружаем уже обученную

        :param path: путь до папки сохранения или загрузки сети
        :param name: имя файла с которого будет считаны (или записанны) параметры
        :param flag: обучать либо загрузить (bool)
        :return:
        """
        # выставляем дефолтные значения
        if path is None:
            path = ''

        if name is None:
            name = 'Net_weights'

        if flag is None:
            print("Укажите режим (True - обучени; False - загрузка)")
            return

        # Обучаем сеть
        if flag is True:
            seed = 7
            np.random.seed(seed)
            self.net.fit(self.X_train, self.y_train, validation_data=(self.X_val, self.y_val), nb_epoch=self.epochs,
                         batch_size=64)

            self.net.save(str(path) + str(name))
            return


        # Загружаем параметры сети
        if flag is False:
            # грузим параметры из фала
            self.net = load_model(str(path) + 'model')
            return


    def pred_and_analysis(self, flag):
        """
        Предсказываем значения из
        :param flag: выводить анализ по сети (bool)
        :return: массив классов распознанных объектов
        """

        # выставляем дефолтные значения
        if flag is None:
            flag = True

        # выполняем предсказание
        preds = self.net.predict(self.X_val)

        if flag is False:
            # возвращаем предсказания
            return preds

        if flag is True:
            print('NOT WORKED YET!')
            return preds
