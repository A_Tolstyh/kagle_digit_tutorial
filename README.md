# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Маленький пример построения свёрточных нейронных сетей для распознавания изображений. 
База взята с kaggle.com, рукописные цифры.

### How do I get set up? ###

Для работы необходимы:

*  python 3.5

*  numpy

*  TensorFlow

*  Keras

*  Theano

*  sklearn

*  matplotlib

*  openCV 3

*  [датасеты](https://www.kaggle.com/c/digit-recognizer/data)

### Theory ###

* [хороший гайд по свёрточным сетям](https://adeshpande3.github.io/adeshpande3.github.io/A-Beginner's-Guide-To-Understanding-Convolutional-Neural-Networks/)

* [книга о глубоком обучении, есть глава по свёрточным нейронным сетям](http://www.deeplearningbook.org/ )

### Train and structure ###

Если добьётесь меньшей ошибки путём изменения структуры сети, делайте коммиты, репозиторий открыт

### Updates ###

* добавлена функция распознавания сетью изображения, загруженного не из датасета

* сеть переложена на TensorFlow'

* сеть обучена, ошибка на валидационных данных 0,007 (абсолютная) [файл весов](https://bitbucket.org/snippets/A_Tolstyh/946X9), точность на тесте Kaggle.com 0.99314